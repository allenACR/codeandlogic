// GLOBAL FUNCTIONS

	////////////////////////
	// CLEAR SECTIONS
	function clearAllSections(){
		$('#pt-main').find('.pt-page').empty();		
	}
	//
	////////////////////////

	
	/////////////////////////////////////////////////////////////
	// LOAD ASSISTANT
	function processLoadOrder(loadOrder, callback ){	

		
		function loopCycle( template, loadInto){
			
			loadOrder.shift();
			$.ajax({
				  url: template,
				  dataType: "html",
				  success: function(data) {
						$(loadInto).append(data);	
						check();	
				  },
				  error: function(){
						callback(false);
				  }
			});	
		
		}
		
		function check(){
		
			
			if (loadOrder.length > 0){
				loopCycle(loadOrder[0][0], loadOrder[0][1]);
			}
			else{
				callback(true);
			}			
		}
		
		check();
		
	}	
	
	////////////////////////  CHANGE SCENES USING HASHTAGS	
	
	function changeHash(newHash, transType){
		
		if (!isScreenAnimation){
				
				if (transType == undefined || transType == null){ transType = 67; } 
				pageTrans = transType;				
				location.hash = "#" + newHash; // get the clicked link id
	
		}
	}
	////////////////////////	
	
	////////////////////////
	function endPageTransistion(){
		$('#contentPage' + priorScreen).empty();
	}
	////////////////////////
		
	//////////////////////
	//  truncateText
	 function truncateText( text, limit ){
		var myText = text.toString();
		len = myText.length;
		
		
		
		if(len > limit)
		{
			return myText.substr(0,limit)+'...';
		}	
		else{		
			return myText;
		}
		
	}
	//		
	///////////////////////	

	//////////////////////
	//  truncateText
	// changeBGLayer('colorSchemeWhite', 100)
	function changeBGLayer(newClass, speed){
		var oldClass = $( '#bgLayer' ).attr('class');
	
		
		 $( '#bgLayer' ).switchClass( oldClass, newClass, speed, "easeInOutQuad" );

	}
	//		
	///////////////////////	
	
	//////////////////////
	//  GO TO TOP	
	function setScrollTop(where, speed){
			
			
			menu = $( '#bt-menu' )
			if (menu !== undefined){
				$(menu).animate(	{ top: where }, speed, function(){	})
			}
			$('html,body').animate(	{ scrollTop: where }, speed, function(){	})
			
			
	}
	//		
	///////////////////////	
	
	
	//////////////////////
	//  GO TO TOP	
	function changeDivColor(type, speed){
			$('html,body').animate(	{ scrollTop: where }, speed, function(){	})
	}
	//		
	///////////////////////		
	
	////////////////////////
	//
	$(window).unbind()	
	$(window).bind('hashchange',function(event){
		updateHashtag();
	} );
		
    function updateHashtag() {
    	
			if (!isScreenAnimation){
				
				changeScreens( getUrlValue(), defaultTrans );  
			}else{
				
				var monitorAnimation = setInterval(function(){
					clearInterval(monitorAnimation);
					setTimeout(function(){
						changeScreens( getUrlValue(), pageTrans );  
					}, 550);
				},1);
			}
	};	
	//
	////////////////////////	
	
	//////////////////////
	//  GETURLVALUE
	function getUrlValue() {

			var parser = document.createElement('a');
			parser.href = window.location;

			parser.protocol; // => "http:"
			parser.hostname; // => "example.com"
			parser.port;     // => "3000"
			parser.pathname; // => "/pathname/"
			parser.search;   // => "?search=test"
			parser.hash;     // => "#hash"
			parser.host;     // => "example.com:3000"

			value = parser.hash.replace(/^.*#/, '');		
			if (value == "" || value == undefined || value == null){ value="" }
				return value 
	}
	//		
	///////////////////////		
	
	


	//////////////////////
	//
	function get_random_color() {
	    var letters = '0123456789ABCDEF'.split('');
	    var color = '#';
	    for (var i = 0; i < 6; i++ ) {
	        color += letters[Math.round(Math.random() * 15)];
	    }
	    return color;
	}
	//
	/////////////////////
	


	
	
	//////////////////////
	//  
	function saveVar(varName, content, callback ) {
				
	 	globalStorage.set( varName, content );
	 	var msg = "(SAVE) " + varName + " = " + content + ""; 
	 	trace(msg, "success");
	 	
	 	if (callback != undefined){
	 		callback();	 		
	 	}
	}
	// saveVar("myName", "Callback", function(){	})  // CALLBACK 
	// saveVar("myName", "NoCallback")					// NO CALLBACK
	/////////////////////	
	
	//////////////////////
	//
	function loadVar(varName, callback) {						
	 	var data = globalStorage.get( varName ); 
	 	
	 	
	 	if (data == undefined){
	 		var msg = varName + " does not exist.";
	 		trace(msg, "error");
	 	}
	 	else{
	 		var msg = "(LOAD) " + varName + " = " + data;
	 		trace(msg, "info");
	 	}	

	 	if (callback != undefined){
	 		callback(data)	; 		
	 	}
	 	else{
	 		return(data);
	 	}
	 	
	}
	// 	loadVar("myName", function(returnData){	})		// WITH CALLBACK
	//  loadVar("myName")								// NO CALLBACK
	/////////////////////		
	
	//////////////////////
	//  
	function deleteVar(varName, callback) {
		globalStorage.remove(varName);
	 	var msg = varName + " deleted.";
	 	trace(msg, "info");		
		
	 	if (callback != undefined){
	 		callback()	 ;		
	 	}			

	}
	// 	deleteVar("myName");
	/////////////////////	
	
	
	
	//////////////////////
	//  GET INFO ON STORAGE TYPE/SIZE  
	function storageInfo(){
		
		// build alert message
		var info = [
		  'Backend: ',
		  Persist.type || 'none',
		  ', ',
		  'Approximate Size Limit: ',
		  (Persist.size < 0) ? 'unknown' : Persist.size 
		].join('');
		
		// prompt user with information
		trace(info);		
		
	}
	
	//////////////////////	
	
	//////////////////////
	// TRACE MESSAGES	
	function trace(msg, type){
		
		if (type == undefined){ type = "trace"}
		
		
		if (debugMode){
			$.bootstrapGrowl(truncateText(msg, 150), {
			  ele: 'body', // which element to append to
			  type: type, // (null, 'info', 'error', 'success')
			  offset: {from: 'bottom', amount: 20}, // 'top', or 'bottom'
			  align: 'right', // ('left', 'right', or 'center')
			  width: 'auto', // (integer, or 'auto')
			  delay: 5000,
			  allow_dismiss: true,
			  stackup_spacing: 30 // spacing between consecutively stacked growls.
			});		
		}
	}
	//
	/////////////////////	



	//////////////////////
	// TRACE MESSAGES	
	function console(msg, type){
		
		if (type == undefined){ type = "trace"}
		

			$.bootstrapGrowl(truncateText(msg, 150), {
			  ele: 'body', // which element to append to
			  type: type, // (null, 'info', 'error', 'success')
			  offset: {from: 'bottom', amount: 20}, // 'top', or 'bottom'
			  align: 'right', // ('left', 'right', or 'center')
			  width: 'auto', // (integer, or 'auto')
			  delay: 10000,
			  allow_dismiss: true,
			  stackup_spacing: 30 // spacing between consecutively stacked growls.
			});	
	
	}
	//
	/////////////////////	
	
	
	//////////////////////
	// FORMAT TIME
	var formatTime = function( timeStamp ) {
			
									var dateString = timeStamp,
									    dateParts = dateString.split(' '),
									    timeParts = dateParts[1].split(':'),
									    date;
									    dateParts = dateParts[0].split('-');
									var month = dateParts[1];
									
									date = dateParts[1] + "-" + dateParts[2] + "-" + dateParts[0];
									return date;

	};	
	//
	/////////////////////	
	
	//////////////////////
	// SCROLL ACTIONS
	function scrollToBottom(){	
		$('html, body').animate({scrollTop:$(document).height()}, 1200);			
	}
	
	function scrollToTop(){
		$('html, body').animate({scrollTop:0}, 1200);	
	}
	//
	/////////////////////	
