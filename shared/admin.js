	var adminURL 	= 'php/phpAdmin.php';


	////////////////////////
	// 
	var User = {
		active: false,
		id: '',
		email: '',
		permissions: '',
		firstName: '',
		lastName: ''
	};
	
	
	
	function getUserInfo(){
	
		var userInfo = JSON.parse(loadVar("userInfo"));

		if (userInfo != undefined || userInfo != null){	
			User.active			= true; 
			User.id 			= userInfo[0];
			User.email	 		= userInfo[1];
			User.permissions	= userInfo[2];
			User.firstName		= userInfo[3];
			User.lastName		= userInfo[4];
		}
		
		
	}
	//
	////////////////////////	

	////////////////////////
	// 
	function beginLogin(iEmail, iPassword ){	

			var iEmail 		= iEmail;
			var iPassword 	= iPassword;  

		       $.ajax({ url: adminURL,
		                 data: {action: 'checkLogin', email: iEmail, password: iPassword},
		                 type: 'post',
		                 success: function(data) {
		                 	if (data == 'BADINPUT'){
		                 		alert("Wrong email or password.  Please try again.");
		                 	}
		                 	else if (data == 'BADCONNECTION'){
		                 		alert("Server error.  Please try again.");
		                 	}
		                 	else{
								saveVar("userInfo", (data) );
								trace("PHP call success.");  	
								location.reload();
							}
		                 },
						 error: function(data){
							trace("PHP call error.");
							alert(data)
						 }
		        });
	}
	//
	////////////////////////		
	
	////////////////////////
	// 
	function beginLogout(){
		
		deleteVar('userInfo');
		
		User = {
			active: false,
			id: '',
			email: '',
			permissions: '',
			firstName: '',
			lastName: ''
		};	
		
		location.reload();	
	}
	//
	////////////////////////	
	
