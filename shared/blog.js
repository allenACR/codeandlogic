	var blogURL 	= 'php/phpBlog.php';
	var killFunction = false; 
	
	

	////////////////////////
	//
    function callPHPFunction(type, callback ){
		killFunction = true; 
		trace(type);
		
       $.ajax({ url: blogURL,
                 data: {action: type},
                 type: 'post',
                 success: function(data) {
                 	killFunction = false; 
					trace("PHP call success.");  
					callback(true, data);	
                 },
				 error: function(data){
					trace("PHP call error.");
					callback(false, data) ;  
				 }
        });
        
        

    }	
	//
	////////////////////////	
	
	// PULL ENTRY //
	function pullEntry(entryNum, callback ){

		//STOPS CURRENT FUNCTIONS
		killFunction = true;
		
       $.ajax({ url: blogURL,
                 data: {action: 'pullEntry', entryId: entryNum},
                 type: 'post',
                 success: function(data) {
                 	killFunction = false; 
					saveVar("blogPull", (data) );
					callback(true, data);	
                 },
				 error: function(data){
					trace("PHP call error.");
					callback(false, data) ;  
				 }
        });	
        
       
	}
	// PULL ENTRY END //
	
	// FETCH ENTRY //
	var ajaxRequest = false;
	function fetchEntries(min, max, callback ){
		
		type = 'pullAllEntry';
		
		//STOPS CURRENT FUNCTIONS
		killFunction = true; 			
			
		// PREVENTS DUPLICATE AJAX REQUESTS
		if(ajaxRequest != false){			
		 	ajaxRequest.abort();
		}
			
	      ajaxRequest = $.ajax({ url: blogURL,
	                 data: {action: type},
	                 type: 'post',
	                 success: function(data) {
	                 	ajaxRequest = false;
						killFunction = false; 	
						saveVar("blogEntries", (data) );
						callback(true);		
	                 },
					 error: function(data){
					 	ajaxRequest = false;
					 	killFunction = false; 	
						trace("Error in server script.");
						callback(false);
					 }
	        });			
				 
			
				 			
	}
	// FETCH ENTRY END //
	
	// SAVE ENTRY //
	function saveEntry(entryDiv){
		
		// GET HTML VALUE
		var contentString 	= convertStringToEscape($('#entryContent' + entryDiv).html());
		var title 			= convertStringToEscape($('#title' + entryDiv).html() 		);
		var author 			= convertStringToEscape($('#author' + entryDiv).html() 		);
		var entryNum		= convertStringToEscape($('#entryNum' + entryDiv).html() 	);
		

		postEntry(entryNum, author, title, contentString, function(returnState, data){
				if (returnState){
					
					console("Entry is updated.");
				}
				else{
					alert("Could not updated.  Please try again.");
				}
		});
		
	
	}
	// SAVE ENTRY END //
	
	// CREATE ENTRY //
	function createNewEntry( author, title, contentString, callback ){
		
		entryNum 		= ""; // LEAVE BLANK - WILL AUTOMATICALLY CREATE A NEW NUMBER

		postEntry(entryNum, author, title, contentString, function(returnState){
				if (returnState){
					console("Entry is created.");
					callback(true);
				}
				else{
					console("Could not updated.  Please try again.");
					callback(false);
				}
		});
	}
	// END CREATE ENTRY //

	// POST ENTRY //
	function postEntry( entryId, authorId, titleId, contentId, callback ){
	
				 trace("Submitting post...");
		         $.ajax({ url: 'php/phpBlog.php',
		                 data: {action: 'postEntry', entryNum: entryId, author: authorId, title: titleId, content: contentId},
		                 type: 'post',
		                 success: function(data) {
							callback(true, data);	
		                 },
						 error: function(data){
							callback(false, data);   
						 }
		        });
		
	};	
	// POST ENTRY END //

	/// BUILD BLOG //
	function buildBlog(entries, min, max, angularId, trunOrNot, size){


		// ESTABLISH ADMIN
		adminRights = false; 
		if (User.permissions == "admin"){
			adminRights = true; 
		}
		//////////////////
		
		
		// GET SCOPE AND COUNT
		counter = min; 
		var scope = angular.element( $('#' + angularId) ).scope() ;	
		//////////////////		
			
			
		// BUILD BLOG		
		function buildBlogModel(i){

			var id = entries[i][0];

			if (!killFunction){	
				//	BUILD HTML
				
				//  IF LARGE
				if (size == "large"){ 
				var shellStart			= '<article id="blogContainer' + id +  '" class="zeroOpacity" ><hr>';
				 	var headerStart			= '<div class="columns" contenteditable="' + adminRights + '">';
				 	var headerImage				= '<img src="http://placehold.it/75x100&text=[pic]" class="textwrap"/>';
				 	var headerTitle				= '<h3><a><span id="title' + id +  '"></span></a></h3>';
				 	var headerMeta				= '<h4 class="gray"><small>Written by <a ><span id="author' + id +  '"></a> | <span id="timeStamp' + id +  '"></small></h4>';
				 	var headerSpacing			= '<br><br>';
				 	var headerClose			= '</div>';
				 	
				 	var contentStart		= '<div class="columns">';
				 	var contentEntryNum			= '<span id="entryNum' + id +  '" class="hide"></span>';
				 	var contentContainer		= '<div class="row">';
				 	var contentClass			= '<div class="large-12 columns white">';
				 	var contentEntry			= '<div id="entryContent' + id +  '" contenteditable="' + adminRights + '"></div>';
				 	var contentEntryClose	= '</div></div>';
				 	var contentSpace		= '<br><br>';
				 	var contentClose		= '</div>';
				      
				    var editStart			= '<div class="columns">';
				    var editUpdate				= '<a onclick="saveEntry(' + id + ')" class="small button floatRight leftIndent edgeMinor">Quick Update</a>';
				    var editEdit				= '<a onclick="saveEntry(' + id + ')" class="small button floatRight leftIndent edgeMinor">Edit</a>';
				    var editSpace			= '<br>';
				    var editClose			= '</div><hr>';
		   
			    var shellClose			= '</article><br>';		
				
				var buildString  = shellStart + headerStart + headerImage + headerTitle + headerMeta + headerSpacing + headerClose;
					buildString	+= contentStart + contentEntryNum + contentContainer + contentClass + contentEntry + contentEntryClose + contentSpace + contentClose; 
					if (adminRights){
						buildString += editStart + editUpdate + editEdit + editSpace + editClose;
					}; 
					buildString += shellClose; 
				}	
				
				//  IF SMALL
				if (size == 'small'){
					var smallShell 		='<dd id="blogContainer' + id +  '">';
					var smallHeader 		='<a href="#panel1"><span id="title' + id +  '"></span><small><br> <span id="author' + id +  '" class="gray"></span> on <span id="timeStamp' + id +  '"></span></small></a>';
					var smallPanel 			='<br><br><div class="content" id="panel1">';
					var smallContent		='<small>';
					var smallEntry			='<div class="white" id="entryContent' + id +  '"></div>';
					var smallClose		='</small></div></dd><br><br>';	
					
					var buildString  = smallShell + smallHeader + smallPanel + smallContent + smallEntry + smallClose; 			
				}
				
				//	BUILD HTML END 
				$('#entryContainer').append(buildString);
				
				
				
				// FADE IN AND CONTINUE
				$('#blogContainer' + id ).animate({opacity: 1}, 250, function(){
				 	
					// EXTRACT FROM CALLBACK
					var entryNum	= entries[i][0];
					var userId 		= entries[i][1];
					var author 		= entries[i][2];
					var title		= entries[i][3];
					var content		= entries[i][4]; 
					var imageLink	= entries[i][5];
					var timeStamp	= formatTime(entries[i][6]);
					
					// TRUNCATE 
					if (trunOrNot){
						author = truncateText(author,  25);
						title = truncateText(title,  10);
						content = truncateText(content, 200);
					};
						
					
					// CONVERT ESCAPE STRINGS TO TEXT										
					content = convertEscapeToString(content);
					title	= convertEscapeToString(title);
					author	= convertEscapeToString(author);	
					
					
					
					// FOR HTML DIVS				
					$('#title' + id).append(title);	
					$('#author' + id).append(author);
					$('#timeStamp' + id).append(timeStamp);
					$('#entryContent' + id).append(content);	
					$('#entryNum' + id).append(entryNum);
				
					//  END OR KEEP LOOPING							
					counter++;					
					if (counter < max){ buildBlogModel(counter); }
					else{	
						
						finishedBuilding(); 
					};
					
				
				 });
			 }else{
			 	finishedBuilding();
			 }
		};
		////// END BUILD
		
		
		////// FINISHED
		function finishedBuilding(){
			angular.bootstrap(document);
			$('#entryLoadScreen').addClass('hide');
			
		};
		///////////////
		
		
		
		buildBlogModel(counter);
	}
	/// BUILD BLOG END //
	
	function validateHTML(html){
		  var doc = document.createElement('div');
		  doc.innerHTML = html;
		  return ( doc.innerHTML === html );		
	}

	
	function convertEscapeToString( string ){
		
		
			var returnString = "";
			
			// escape to apostrophe 
			var str = string, reg = /&apsh/ig;									
			returnString = str.replace(reg, "`");  											

			// escape double quotes
			var str = returnString, reg = /&qta/ig;									
			returnString = str.replace(reg, "\""); 	
				
			// Enter to escape
			var str = returnString, reg = /&entr/ig;									
			returnString = str.replace(reg, "<br>"); 

			
			return(returnString);
	};
	
	

	
	function convertStringToEscape( string ){
		
			var returnString = "";
			
			// apostrophe to escape 
			var str = string, reg = /'/ig;									
			returnString = str.replace(reg, "&apsh");  				

			// Quote to escape
			var str = returnString, reg = /"/ig;									
			returnString = str.replace(reg, "&qta"); 
			
			// Enter to escape
			var str = returnString, reg = /\n/ig;									
			returnString = str.replace(reg, "&entr"); 				
			
			return(returnString);
		
	}
	
	


