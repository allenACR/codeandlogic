<?php


require '../settings/phpsettings.php';
	
	
	
//  PARSE THE CALL	
//
if(isset($_POST['action']) && !empty($_POST['action'])) {

    $action = $_POST['action'];
	
	
	
    switch($action) {
		case 'postEntry'			: postEntry( $_POST['entryNum'], $_POST['author'], $_POST['title'], $_POST['content'] );break;
		case 'countPosts'			: countPosts(); break;		
		case 'testFunction'			: testFunction(); break;
        case 'connectionStatus' 	: connectionStatus();break;	
		case 'pullEntry'			: pullEntry( "author", $_POST['entryId'] );break; 
		case 'pullAllEntry'			: pullEntry( "author", "entryNum" );break;  											// author field can be left blank to filter for all, entryNum can be a specific number
		case 'sendEmail'			: sendEmail( $_POST['name'], $_POST['email'], $_POST['contact'] );break;
		case 'getLastEntry'			: getLastEntry(); break; 
    }
}	
//
///////////



function testFunction(){
	
	global $val_to_print;
	$val_to_print .= ("Test successful"); 

	die ($val_to_print); 
}

function connectToDatabase(){

	global $val_to_print;
	global $link;
	global $dbBlog;
	
  
	// ENSURE LINK TO SERVER IS GOOD
		if (!$link) {
			$val_to_print .= ('Could not establish link.  ');
			return false;
		}
	//
	
	// CHECK INDIVIDUAL DATABASES
		$db_selected = mysql_select_db($dbBlog);
		if (!$db_selected) {
		  $val_to_print .= ('Could not connect to blog database.  ');
		  return false;
		}
	// 
	
	// IF ALL IS GOOD, RETURN TRUE
	return true; 
	
}
		
function connectionStatus(){

	global $val_to_print;
	
	if( connectToDatabase() ){				
		$val_to_print .= ('All databases connected successfully.');
		echo ($val_to_print);
	}	
	else{
		$val_to_print .= ('Error in connecting to the database structure.');
		echo ($val_to_print);
	};
}



function countPosts(){

	// SET VARIABLES
	global $val_to_print;
	global $link;
	global $dbBlog;
	
	// CHECK FOR CONNECTION 
	if(connectToDatabase() ){				
	
		// SELECT THE BLOG DATABASE
		$db_selected = mysql_select_db($dbBlog);  // 
		
				//  PREPARE TO ENTER SQL DATABASE
				$result = mysql_query("SELECT * FROM entries ");
				$totalEntries = mysql_num_rows($result);
				
				$val_to_print = ($totalEntries);
				echo ($val_to_print); 

	}
	else{
		$val_to_print .= ('Error in connecting to the database structure.');
		echo ($val_to_print); 
	}	
	
}


function postEntry( $entryNum, $author, $title, $content ){

	// SET VARIABLES
	global $val_to_print;
	global $link;
	global $dbBlog;
	
	$imageLink	= '';
	$editDate	= '0000-00-00 00:00:00';
	
	// CHECK FOR AUTHOR REQUIREMENT
	//
	if( $entryNum == "" || $entryNum == "entryNum") {
			$sql = 	'INSERT INTO entries '. 
				   	'(author, title, content, imageLink, editDate) '. 
			   		'VALUES ( "'. $author .'","' . $title . '","'. $content .'","'. $imageLink .'","'. $editDate . '"  )';		

	}
	else{
			$sql = 	'UPDATE entries ' .
					'SET title="' . $title . '", author="' . $author . '", content="' . $content . '"' .
					'where entryNum = "' . $entryNum . '"';
	}
	//
	///////////////////////////////	
	
	//die ( $$sql );
	
	// CHECK FOR CONNECTION 
	if(connectToDatabase() ){				
	
		// SELECT THE BLOG DATABASE
		$db_selected = mysql_select_db($dbBlog);  // 
		
				//  PREPARE TO ENTER SQL DATABASE


				$retval = mysql_query( $sql, $link );
				if(! $retval )
				{
					$val_to_print .= ( 'Could not enter data: ' . mysql_error() );
					echo ($val_to_print); 
				}
				else{
					$val_to_print .= ( 'Entry successfully posted.' );
					echo ($val_to_print); 	
				}

	}
	else{
		$val_to_print .= ('Error in connecting to the database structure.');
		echo ($val_to_print); 
	}
	
}



function getLastEntry(){

	// SET VARIABLES
	global $val_to_print;
	global $link;
	global $dbBlog;

	$db_selected = mysql_select_db($dbBlog);  // 
	$result = mysql_query("SELECT MAX(entryNum) FROM entries");
	
		if (!$result) {
			$val_to_print = ('Could not query: ' . mysql_error());
		}
		// IF QUERY FAILS
		else{			
			$val_to_print = mysql_result($result, 0) ;  // ONLY ONE ITEM SHOULD BE RETURNED, HENCE, USE 0
			echo($val_to_print);
		}	

}




function pullEntry(  $author, $entry  ){
	
	
	
	// SET VARIABLES
	global $val_to_print;
	global $link;
	global $dbBlog;
	
	
	// CHECK FOR AUTHOR REQUIREMENT
	//
	if( $author == "" || $author == "author") {
		$authorBuildString = "";
	}
	else{
		$authorBuildString = "WHERE author = '" . $author . "'";
	}
	//
	///////////////////////////////
	
	// CHECK FOR ENTRY REQUIREMENT
	//
	if( $entry == "" || $entry == "entryNum") {
		$entryNum = "ALL";
	}
	else{
		$entryNum = $entry;
	}
	//
	///////////////////////////////	
	
	
	
	// CHECK FOR CONNECTION 
	if(connectToDatabase() ){				
	
		// SELECT THE BLOG DATABASE
		$db_selected = mysql_select_db($dbBlog);  // 
		
			
				// IF RETRIEVING ALL ENTRIES
				if ($entryNum == "ALL"){
				
					// SELECT THE ENTRIES TABLE
					$result = mysql_query("SELECT * FROM entries " . $authorBuildString . "ORDER BY entryNum ");
					
					// QUERY FAILS
					if (!$result) {
						$val_to_print .= ('Could not query: ' . mysql_error());
					
					}
					// IF QUERY FAILS
					else{					
						$totalEntries = mysql_num_rows($result);
							// FOR LOOP
							$val_to_print .= "[";
							for ($i=0; $i<= $totalEntries - 1; $i++){
								$val_to_print .= "[\"";
								$val_to_print .= mysql_result($result, $i, 'entryNum') 		. "\",\"" . 
												 mysql_result($result, $i, 'userId') 		. "\",\"" . 
												 mysql_result($result, $i, 'author') 		. "\",\"" . 
												 mysql_result($result, $i, 'title') 		. "\",\"" . 
												 mysql_result($result, $i, 'content') 		. "\",\"" . 
												 mysql_result($result, $i, 'imageLink') 	. "\",\"" . 
												 mysql_result($result, $i, 'creationDate') ;		
								$val_to_print .= "\"]";
								if ($i < $totalEntries - 1){ $val_to_print .= ",";	};							
							};
							$val_to_print .= "]";
						//$val_to_print = '[["Allen Royston","Entry 0", "Content &apsf and here we are"]]';
					
						echo ($val_to_print); 
					};						
				}


				// RETURN JUST A SPECIFIC ENTRY
				else{
					// SELECT THE ENTRIES TABLE
					$result = mysql_query("SELECT * FROM entries " . $authorBuildString . "WHERE entryNum = " . $entryNum);
					
					
					
					// QUERY FAILS
					if (!$result) {
						$val_to_print .= ('Could not query: ' . mysql_error());
					
					}
					else{
						$i = 0; // SINCE ONLY ONE ENTRY COMES UP, PICK THE FIRST ONE
							$val_to_print .= "[[\"";
								$val_to_print .= mysql_result($result, $i, 'entryNum') 		. "\",\"" . 
												 mysql_result($result, $i, 'userId') 		. "\",\"" . 
												 mysql_result($result, $i, 'author') 		. "\",\"" . 
												 mysql_result($result, $i, 'title') 		. "\",\"" . 
												 mysql_result($result, $i, 'content') 		. "\",\"" . 
												 mysql_result($result, $i, 'imageLink') 	. "\",\"" . 
												 mysql_result($result, $i, 'creationDate') ;		
								$val_to_print .= "\"]";						
							$val_to_print .= "]";
							
					echo ($val_to_print);
					};					
				}

				
	}
	// CONNECTION FAILS
	else{
		$val_to_print .= ('Error in connecting to the database structure.');
		echo ($val_to_print); 
	};

	

}




?>