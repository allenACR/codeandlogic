<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>Foundation | Welcome</title>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
	
	
			<!-- REQUIRED CSS SCRIPTS / MUST BE INCLUDED BEFORE BODY -->
				<link type="text/css" href="plugins/foundation5/css/foundation.css" 			rel="stylesheet"/>	
				<link type="text/css" href="plugins/pageTransistions/css/multilevelmenu.css" 	rel="stylesheet" />			<!-- required -->
				<link type="text/css" href="plugins/pageTransistions/css/component.css" 		rel="stylesheet" />			<!-- required -->
				<link type="text/css" href="plugins/pageTransistions/css/animations.css"		rel="stylesheet" />		
				<link type="text/css" href="plugins/colorBox/css/colorbox.css"					rel="stylesheet" />			<!-- required -->				
				
				
				<!-- PLUGINS -->				
				<link type="text/css" href="plugins/jmpress/css/style.css" 						rel="stylesheet"/>		<!-- optional jsmpress -->								
				<link type="text/css" href="plugins/custom/css/stylesheet.css"					rel="stylesheet"/>		<!-- required CUSTOM included at end of CSS-->		
			<!----->
	
			<!-- REQUIRED JS SCRIPTS / MUST BE INCLUDED BEFORE BODY -->	
				<script src="plugins/pageTransistions/js/modernizr.custom.js"></script>									<!-- required for several plugins -->
				<script src="plugins/angular/js/angular1.0.8.js"></script>												<!-- required -->
				<script src="plugins/persistJS/js/persist-all-min.js"></script>											<!-- required -->
				<script src="plugins/callRequests.js"></script>															<!-- required -->	
				
				<script src="shared/admin.js"></script>	
				<script src="shared/blog.js"></script>	
			<!----->
	
			<style>
				#pt-main{
					top: 48px; 
				}			
			</style>
			
			
	
  </head>
  <body style="min-width: 800px">
    
	
		<!-- PERSIST THROUGH TRANSISTIONS -->
		<div id="persistLayer" class="fixed">
		</div>	
		<!-- END -->
		


		<!-- BACKGROUND LAYER -->
		<div id="bgLayer" class="blackGradiant" style="width: 100%; height: 100%; position: fixed">
			
				 <div class="large-12 columns">
					<!-- <img src="siteImages/background1.jpg" width="100%"/> -->
				</div>
			
		
		</div>
		<!-->

		<!-- CONTENT GOES HERE -->
		<div id="pt-main" class="pt-perspective">

		
			<div id="contentPage1" class="pt-page pt-page-1 " >					
			</div>
			<div id="contentPage2" class="pt-page pt-page-2 ">	
			</div>

					
			
		</div>
		<!-- END -->	
	
	
		<!-- LOGIN MODAL -->
		<div id="loginModal" class="reveal-modal" data-reveal> 
			<form>
			  <fieldset>
				<legend>Login</legend>
	
				<div class="row">
				  <div class="large-12 columns">
					<label>Email <small>required</small></label>
					<input id="loginEmail" type="text" required="" name="email" placeholder="" >	
				  </div>
				  <div class="large-12 columns">
					<label>Password <small>required</small></label>
					<input id="loginPassword" type="password" required="" name="password" placeholder="" >
				  </div>
				</div>
	
			  </fieldset>
			</form>
			
      		<a class="small edgeMinor button floatRight leftIndent" onclick="beginLogin( $('#loginEmail').val(), $('#loginPassword').val() )">Login</a>        	
			<a class="close-reveal-modal">&#215;</a>
		</div>		
		<!-- END LOGIN PORTAL -->
		
		
		<!-- LOGOUT MODAL -->
		<div id="logoutModal" class="reveal-modal" data-reveal> 
			<h2 class="black">LOGOUT?</h2>
			
      		<a class="small edgeMinor button floatRight leftIndent" onclick="beginLogout()">Log me out</a>        	
			<a class="close-reveal-modal">&#215;</a>
		</div>		
		<!-- END LOGIN PORTAL -->		
    

  </body>
  
	<!-- LOAD PLUGINS -->
				<script src="settings/settings.js"></script>										<!-- required -->
				<script src="plugins/foundation5/js/jquery.js"></script>							<!-- required -->
				<script src="plugins/jQuery/jquery-ui-1.10.3.custom.js"></script>					<!-- required -->
				<script src="plugins/foundation5/js/foundation.min.js"></script>					<!-- required -->
				<script src="plugins/foundation5/js/foundation/foundation.dropdown.js"></script>	<!-- required -->
				<script src="plugins/growl/js/jquery.bootstrap-growl.js"></script>					<!-- required growl -->										
				
				<script src="plugins/pageTransistions/js/jquery.dlmenu.js"></script>				<!-- required for transitions -->
				<script src="plugins/pageTransistions/js/pagetransitions.js"></script>				<!-- required for transitions -->
				<script src="plugins/custom/js/global.js"></script>									<!-- required -->	

				<script src="plugins/jmpress/js/jmpress.min.js"></script>							<!-- optional *jmpress -->
				<script src="plugins/jmpress/js/jquery.jmslideshow.js"></script>					<!-- optional *jmpress -->	
				

				<script src="plugins/stapel/js/jquery.stapel.js"></script>							<!-- optional *stapel -->
				

	<!----->
	

	
	
	
	
    <script>


	
	
	///// REFRESHINIT
	function refreshInit(){
		
		
		$('#persistLayer').empty();
		
	
		/////////////////////////////////////////////////////////////
		//  LOAD ORDER	
			  loadOrder = [	
					['views/navBar.html',		'#persistLayer']
				];			

			///////////////
			//  PROCESS LOAD ORDER
			processLoadOrder(loadOrder, function(returnState){
				if (returnState){
					clearAllSections();
					setTimeout( function(){	
						if ( getUrlValue() == ""){ 
							changeHash('home');
						} 
						else { 											
							changeScreens( getUrlValue()  ); 		
						} 	
					}, 200); 	
				}
				else{
					alert("Page could not load AJAX files.  If you are using Chrome, disable security mode or use Firefox instead.");	
				}	
			});
		
		/////////////////////////////////////////////////////////////
		//  START
				
	}
	//  INIT END
	
	
	
	// CHANGE SCENES
	var currentScreen 	= 1;
	var priorScreen		= 0;	 
	var defaultTrans	= 33; // DEFAULT PAGE TRANSISTION TYPE
	var firstLoad		= false;
	function changeScreens(type, transType){
		
	

		if (transType == undefined || transType == null){ transType = defaultTrans } 
			priorScreen = currentScreen;
			currentScreen ++;		
		if (currentScreen > 2){ currentScreen = 1; } 
			nextPageTrans(transType);
		
	
	
		var loadOrder;
		////////////////////  
		//
		
		if (type == "home" || type == ""){	
			
				loadOrder = [		
					['views/main.html',					'#contentPage' + currentScreen],	
				];
				
		}	
		else{								
				
				loadOrder = [	
					['views/' + type + '.html', 		'#contentPage' + currentScreen]	
				]	;	
		};			

		//
		////////////////////  
		if (loadOrder)
		
		
		processLoadOrder(loadOrder, function(returnState){
				
			if (returnState){
				$(document).foundation();								//FOUNDATION 5 INITILIZATION 	
				if (!firstLoad){ firstLoad = true; loadCompleted(); }
			}
			else{
				alert("Page could not load AJAX files.  If you are using Chrome, disable security mode or use Firefox instead.");
				
			}
			
		});
		
	}		
	//
	////////////////////////	

	


	

	
	

	
	
	/////  INIT
	//
	getUserInfo();
	$('body').fadeOut( 0 );
	var rootStorage = new Persist.Store('root', {});
		if (rootStorage.get("headerType") == undefined){ rootStorage.set("headerType", "offCanvas");	}
	
	$().ready(function() {		
		refreshInit();     
    });		
    
	//
	/////	
	
	/////  LOAD COMPLETE
	//	
	function loadCompleted(){	
			initCheck();
			//changeBGLayer('colorSchemeC', 3000)					
			//mainLoader.show()
			
			$('body').fadeIn( 500, function(){		
				$('body').css( 'overflow-y', 'auto' );	
			});
			

	}
	//
	/////	



    
	  
	  
    </script>  
  
  
</html>
